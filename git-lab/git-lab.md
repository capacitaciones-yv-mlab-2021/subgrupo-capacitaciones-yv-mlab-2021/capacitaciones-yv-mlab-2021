# GITLAB

# MENU
- # [Regresar al Menu](../README.md)

- [GitLab informacion de como accedo](#gitlab)

- [Crear Repositorios](#repositorios)

- [Grupos](#grupos)

- [Issues](#issues)

- [Labels](#labels)

- [Permisos](#permisos)

- [Miembros](#miembros)

- [Boards](#boards)

- [Commits](#commits)


# GitLab

# GitLab informacion de como accedo
Para poder acceder al GitLab seguiremos estos pasos

- Primero me dirigire a la pagina principal del GitLab

- Y me registrare con el correo institucional

- Una vez registrado ire al kraken y ire a configuraciones 

![12](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/registro1.jpg)

- Me dirigire a Integrations e ire a la parte de Git Lab, una vez ahi pondre vicular cuenta y se vinculara con la cuenta del GitLab y eso sera todo para poder facilitar la subida de archivos

![12](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/registro2.jpg)

![12](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/registro3.jpg)



- [Ir al Menu Principal](#menu)

# Repositorios
El apartado donde creare mi proyecto es el repositorio
## Crear Repositorios

- 
![repositorio-1](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/repositorio.jpg)

- 
![repositorio-2](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/repositorio2.jpg)

- [Ir al Menu Principal](#menu)

# Grupos

### Ire a crear grupo y lo crearemos con los datos que nos pide
- 
![Grupos](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/grupo1.jpg)

- 
![Grupos](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/grupo2.jpg)

- 
![Grupos](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/grupo3.jpg)


- [Ir al Menu Principal](#menu)

# Crear Subgrupos

- 
![Grupos](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/grupo3.jpg)

- [Ir al Menu Principal](#menu)

# Issues

## Crear Issues
Para crear Issues nos dirigimos a Issues y damos en crear

- 
![Issue](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/issues.jpg)

- [Ir al Menu Principal](#menu)

# Labels

- 
![Labels](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/Labels.jpg)

- 
![Labels](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/Labels2.jpg)

- 
![Labels](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/Labels3.jpg)

- 
![Labels](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/Labels4.jpg)

- [Ir al Menu Principal](#menu)

# Permisos

## **Hablar sobre los permisos, cuantos hay y para que sirven**

EXISTEN 4 ROLES O PERMISOS QUE SE OTORGA A LA PERSONA QUE SE INVITA LOS CUALES SON

- GUEST
- REPORTER
- DEVELOPER
- MAINTAINER

MAINTAINER ES PRACTICAMENTE COMO GOLD CASI CON LOS MISMOS CRITERIOS.

DEVELOPER LA DIFERENCIA ES QUE YA NO SE PODRA HACER COMMITS EN LA RAMA MASTER.

- [Ir al Menu Principal](#menu)

# Miembros

Pasos para agregar miembros al gitlab

- ![miembros](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/Ramma-2/fotos/miembro.jpg)

- ![miembros](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/Ramma-2/fotos/miembro2.jpg)

- ![miembros](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/Ramma-2/fotos/miembro3.jpg)

- ![miembros](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/Ramma-2/fotos/miembro4.jpg)


- [Ir al Menu Principal](#menu)

# Boards
Pasos para crear boards

- ![Boards](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/BOARD.jpg)

- ![Boards](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/BOARD2.jpg)

- ![Boards](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/BOARD3.jpg)


- [Ir al Menu Principal](#menu)
# Commits

### Commits desde gitlab y crear ramas desde gitlab

- ![Commits-gitlab](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/Ramma-2/fotos/COMMITGIT.jpg)

### Ramas


- ![Rammas-gitlab](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/ramasgitlab.jpg)

- ![Rammas2-gitlab](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/ramas2.jpg)

- [Ir al Menu Principal](#menu)

[Regresar al Menu Principal](../README.md)
