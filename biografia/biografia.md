# BIOGRAFIA
#### BRAYAN GANAN

![foto-brayan-ganan](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/foto.jpg)


Mi nombre es **Brayan Ganan**, tengo 18 años, nací el 20 de mayo del año 2002, mi mamá se llama Rosa Santafe y mi papá se llama Marco Ganan, por razones del destino no vivo con mi papá pero crecí con un padrastro y tuve una hermana llamada Cristina, mi papá tuvo otra relación y fruto de ello tuvo una hija por lo tanto tengo dos hermanastras, desde muy pequeño viví al sur de Quito en la Lucha de los Pobres, a la edad de 5 años me mude al mayorista y viví gran parte de mi adolescencia en ese lugar, estudie en una escuela llamada Eduardo Vázquez Dodero.

Salí de la escuela y comencé mis estudios en la INSTITUCION EDUCATIVA FISCAL “SUCRE”, no fui el mejor estudiante ni el peor pero siempre mantuve un buen promedio y nunca me he quedado a supletorios, en mi casa fueron muy severos con mi educación y gracias a eso nunca me desvié, hubieron personas que a diferencia mía preferían ir a fiestar y tomar y yo por lo contrario prefería estar en mi casa pudiendo jugar algo o investigando algo, siempre me gusto saber mucho sobre las computadoras.
Pude graduarme del colegio pero sufrimos una pandemia no tuve ceremonia de graduación pero eso no me impidió poder sentirme bien por haber cumplido una meta más, di el examen del EAES para poder acceder a un cupo a la universidad o un instituto y gracias al esfuerzo que dedique pude obtener un cupo en el Instituto Benito Juárez próximo a llamarse “Instituto de Turismo y Patrimonio Yavirac” y ahora estoy cursando la carrera de desarrollo de software.

En mis tiempos libres me gusta mucho jugar video juegos, tocar la guitarra e investigar sobre software, computadoras, ensamblajes, etc.
Tambien me gusta jugar con mis mascotas, sonara exagerado pero aproximadamente tengo 9 perros, 4 son rescatados y los 5 son chihuahuas, proximamente tendre un gato y completare mi familia de animales, olvide mencionar que tengo tambien un conejo y que se ha criado conmigo desde que estoy en el colegio.

[Regresar al Menu](../README.md)