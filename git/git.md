# GIT

# MENU
- # [Regresar al Menu](../README.md)


- [Que es Git](#git)

- [Comandos de Git en la consola](#comandos)

- [Clientes](#clientes)

- [Clonacion](#clonacion)

- [Commits](#commits)

- [Ramas](#ramas)

- [Biografia](/biografia/biografia.md)



# Git
## Que es Git

Git es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplicaciones cuando estas tienen un gran número de archivos de código fuente.

- [Menu Principal](#menu)

# Comandos
## Comandos de Git en la consola
- Git init

- Git touch

- Git add "nombre del archivo"

- Git commit -m "mensaje personalizado"

- touch .gitignore

- git branch rama-1

- git status

- git clear

- [Menu Principal](#menu)

# Clientes

- SmartGit : SmartGit es una herramienta multiplataforma de entorno fácil e intuitivo que nos ayuda con Git. Veamos como empezar a trabajar con esta herramienta usando Git y el alojamiento GitHub.

- ![smart](https://static.filehorse.com/screenshots/developer-tools/smartgit-screenshot-01.png)

- Kraken : GitKraken es un cliente Git construido con electrón. Esto permite que se ejecute de forma nativa en Gnu/Linux, Windows y Mac. Esta aplicación es un interesante cliente de Git que ofrece una interfaz moderna con un entorno sólido para trabajar con repositorios que además nos proporciona un conjunto de características básicas.

- ![kraken](https://www.gitkraken.com/img/index/gk-product-2.png)

- [Menu Principal](#menu)


# Clonacion
## Clonacion de proyecto por consola y por cliente

- 
![imagen-principal](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/clonacion.jpg)

- 
![imagen-principal](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/clonacion%202.jpg)

- [Menu Principal](#menu)


# Commits
## Commits por consola y por cliente kraken o smart

- ![commit](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/commit.jpg)

- ![commit](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/commit1.jpg)

- ![commit](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/master/fotos/commit2.jpg)


# Ramas
## Ramas desde Kraken

Una rama Git es simplemente un apuntador móvil apuntando a una de esas confirmaciones. La rama por defecto de Git es la rama master . Con la primera confirmación de cambios que realicemos, se creará esta rama principal master apuntando a dicha confirmación.

- ![rama](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/Ramma-2/fotos/ram.jpg)

## Merge
Realiza una fusión a tres bandas entre las dos últimas instantáneas de cada rama y el ancestro común a ambas, creando un nuevo commit con los cambios mezclados.

- ![Merge](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/Ramma-2/fotos/merge.jpg)

- ![Merge](https://gitlab.com/capacitaciones-yv-mlab-2021/subgrupo-capacitaciones-yv-mlab-2021/capacitaciones-yv-mlab-2021/-/raw/Ramma-2/fotos/merge2.jpg)

- [Menu Principal](#menu)


- # [Regresar al Menu Princpial](../README.md)